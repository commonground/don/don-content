# Contributing to the content of https://developer.overheid.nl

The content of API's and repositories is stored as `.yml` files with a specific
schema:

- The schema for API's is: [schema/don-api.json](schema/don-api.json)
- The schema for repositories is:
  [schema/don-repositories.json](schema/don-repositories.json)

## Creating files with the right schema

The schema for repositories is quite simple to get right, but the schema for
API's is more complex. The easiest way to add an API with the correct schema, is
to make a submission through our [new API
form](https://developer.overheid.nl/apis/toevoegen). After you make the
submission we will manually verify it and add it to the content repository.

## Linting content files

Before merging new or edited content files to the main branch, we lint the files
to make sure they have a valid schema and content. All these checks can be found
in the `.gitlab-ci` file. You can also perform these checks yourself while
editing the files locally.

### Setup

Install yamllint

```sh
apt install yamllint
```

Create a virtual environment and install python packages (using python >=
3.11)

```sh
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

### Run

Check that the yaml syntax is correct

To check if repistory .yaml files are correct, run:

```sh
yamllint -c .yamllint.yaml ./content/repository/
```

```sh
yamllint -c .yamllint.yaml <directory or file>
```

Check that the schema of an API file is correct

```sh
scripts/validate-schema.py schema/don-api.json <directory or file>
```

Check that the schema of a repositories file is correct

```sh
scripts/validate-schema.py schema/don-repositories.json <directory or file>
```

Check that the data of an API file is correct

```sh
scripts/validate-api.py <directory or file>
```

Check that the data of a repositories file is correct

```sh
scripts/validate-repositories.py <directory or file>
```

Check that the id and name of an organisation is correct

```sh
scripts/validate-organization.py <file>
```

